import models.Account;
import models.Customer;

public class App {
    public static void main(String[] args) throws Exception {
        Customer customer1 = new Customer(1, "Thang", 1000000);
        System.out.println("customer 1");
        System.out.println(customer1);

        Customer customer2 = new Customer(2, "Dung", 2000000);
        System.out.println("customer 2");
        System.out.println(customer2);

        Account account1 = new Account(1, customer1, 1000000);
        System.out.println("account 1");
        System.out.println(account1);

        Account account2 = new Account(2, customer2, 2000000);
        System.out.println("account 2");
        System.out.println(account2);
    }
}
